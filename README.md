*** README ***

Run hello-openshift

 ``` oc new-app https://gitlab.com/rcardona/ocp4-apps-examples.git --context-dir=hello-openshift --name my-test-app ```

Run running-on-host-nodejs

``` oc new-app nodejs:10~https://gitlab.com/rcardona/ocp4-apps-examples.git --context-dir=running-on-host-nodejs --name my-test-app ```
